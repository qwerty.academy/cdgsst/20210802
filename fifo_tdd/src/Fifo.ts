export default class Fifo{
    
    private estado: string[];

    constructor()  {
        this.estado = [];
    }

    retira(): string {
        if(this.vazia()) 
            return "ESTOU COM FOME";

        return this.alteraEstadoRetornaPrimeiro()
    }

    private alteraEstadoRetornaPrimeiro(): string {
        const [ primeiro, ...resto] = this.estado
        this.estado = resto

        return primeiro;
    }

    private vazia() {
        return this.tamanho() == 0;
    }

    introduz(valor?: string) {
        if(valor) {
            this.estado.push(valor);
        }
    }

    tamanho(): number {
        return this.estado.length;
    }
}