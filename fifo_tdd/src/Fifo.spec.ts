import Fifo from "./Fifo"

describe("Implementação da funcionalidade de Fila", () => {
    test("Ao introduzir 'A' numa fila nova e retirar logo a seguir, deve-se retornar 'A'", () => {
        // Arrange
        const fifo = new Fifo();
        fifo.introduz("A");

        // Act
        const resultado  = fifo.retira();

        // Assert
        expect(resultado).toStrictEqual("A");
    });

    test("Ao tentar retirar de uma fila vazia, retornar 'ESTOU COM FOME'", () => {
        // Arrange
        const fifo = new Fifo();

        // Act
        const resultado  = fifo.retira();

        // Assert
        expect(resultado).toStrictEqual("ESTOU COM FOME");
    });

    test("Ao inserir null|undefined, não deve alterar o estado da fila", () =>{
        // Arrange
        const fifo = new Fifo();
        fifo.introduz("B");
        fifo.introduz();

        // Act
        const tamanho = fifo.tamanho()

        // Assert
        expect(tamanho).toStrictEqual(1); 
    });

    test("Ao introduzir X,Y,Z o tamanho deve ser 3", () =>{
        // Arrange
        const fifo = new Fifo();
        fifo.introduz("X");
        fifo.introduz("Y");
        fifo.introduz("Z");

        // Act
        const tamanho = fifo.tamanho()

        // Assert
        expect(tamanho).toStrictEqual(3); 
    });

    test("Ao introduzir O,P,Q; ao retirar deve receber O", () =>{
        // Arrange
        const fifo = new Fifo();
        fifo.introduz("O");
        fifo.introduz("P");
        fifo.introduz("Q");

        // Act
        const resultado = fifo.retira()

        // Assert
        expect(resultado).toStrictEqual("O"); 
    });

    test("Ao introduzir E ,F, G,H; ao retirar duas vezes, deve retornar G logo a seguir", () =>{
        // Arrange
        const fifo = new Fifo();
        fifo.introduz("E");
        fifo.introduz("F");
        fifo.introduz("G");
        fifo.introduz("H");
        fifo.retira()
        fifo.retira()


        // Act
        const resultado = fifo.retira()

        // Assert
        expect(resultado).toStrictEqual("G"); 
    });
});