import startHttpServer from "./presentation/http/http_server.ts";

if (import.meta.main) {
	startHttpServer();
}
