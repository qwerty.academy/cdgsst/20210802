import CarTypeRepository from "../domain/car_type_repository.ts";
import Coordinates from "../domain/coordinates.ts";
import DistanceCalculator from "../domain/distance_calculator.ts";
import Rides, { RideOption } from "../domain/rides.ts";

export default class RidingService {
	constructor(
		readonly distanceCalculator: DistanceCalculator,
		readonly carTypeRepository: CarTypeRepository
	) {}

	async getRideOptions(pickup: string, dropoff: string): Promise<RideOption[]> {
		const startCoord = Coordinates.from(pickup).value as Coordinates;
		const endCoord = Coordinates.from(dropoff).value as Coordinates;

		const cars = await this.carTypeRepository.getAll();

		return Rides.getOptions(cars, startCoord, endCoord, this.distanceCalculator);
	}
}
