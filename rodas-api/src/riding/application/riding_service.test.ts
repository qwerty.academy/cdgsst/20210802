import { assertEquals, spy, assertSpyCalls } from "../../dev_deps.ts";
import CarType from "../domain/car_type.ts";
import Coordinates from "../domain/coordinates.ts";
import RidingService from "./riding_service.ts";

Deno.test("Riding service :: get ride options :: should call distance calculator", async () => {
	const pickup = "-8.817956,13.232034";
	const dropoff = "-8.817056,13.272034";

	const calculator = { calculate: spy((_a: Coordinates, _b: Coordinates) => 2.75) };

	const carTypeRepository = {
		getAll: () => {
			return Promise.resolve([new CarType("JEEP", 1000), new CarType("ECCO", 750)]);
		},
	};

	const ridingService = new RidingService(calculator, carTypeRepository);

	const options = await ridingService.getRideOptions(pickup, dropoff);

	assertSpyCalls(calculator.calculate, 1);
	assertEquals(options.length, 2);
});
