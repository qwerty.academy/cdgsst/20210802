export default class CarType {
	constructor(readonly name: string, readonly costPerKm: number) {}
}
