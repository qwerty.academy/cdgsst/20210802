import Coordinates from "./coordinates.ts";

export default interface DistanceCalculator {
	calculate(pickup: Coordinates, dropoff: Coordinates): number;
}
