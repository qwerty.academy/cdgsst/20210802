import CarType from "./car_type.ts";
import Coordinates from "./coordinates.ts";
import Rides from "./rides.ts";

import { assertEquals, spy } from "../../dev_deps.ts";

Deno.test("rides :: list ride options ::", () => {
	const cars = [new CarType("SUV", 1200), new CarType("ECONOMIC", 950)];
	const pickup = Coordinates.from("-8.817956,13.232034").value as Coordinates;
	const dropoff = Coordinates.from("-8.817056,13.272034").value as Coordinates;

	const calculator = { calculate: spy((_a: Coordinates, _b: Coordinates) => 2.5) };

	const options = Rides.getOptions(cars, pickup, dropoff, calculator);

	assertEquals(calculator.calculate.calls.length, 1);
	assertEquals(options[0], { estimatedCost: 3000, distanceInKm: 2.5, rideType: "SUV" });
});

Deno.test("rides :: list ride options :: with other values", () => {
	const cars = [new CarType("LUXURY", 650), new CarType("GREEN", 450)];
	const pickup = Coordinates.from("-8.817956,13.232034").value as Coordinates;
	const dropoff = Coordinates.from("-8.817056,13.272034").value as Coordinates;

	const calculator = { calculate: spy((_a: Coordinates, _b: Coordinates) => 5) };

	const options = Rides.getOptions(cars, pickup, dropoff, calculator);

	assertEquals(options, [
		{ estimatedCost: 3250, distanceInKm: 5, rideType: "LUXURY" },
		{ estimatedCost: 2250, distanceInKm: 5, rideType: "GREEN" },
	]);
});
