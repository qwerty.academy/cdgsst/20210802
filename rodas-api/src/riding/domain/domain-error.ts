export default interface DomainError {
	error(): string;
}
