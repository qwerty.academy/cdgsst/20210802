import DomainError from "./domain-error.ts";
import { Either, success } from "../../shared/either.ts";

export default class Coord {
	readonly lat: number;
	readonly long: number;

	private constructor(value: string) {
		[this.lat, this.long] = value.split(",").map((v) => parseFloat(v));
	}

	static from(value: string): Either<Coord, InvalidCoordFormatError> {
		return success(new Coord(value));
	}
}

class InvalidCoordFormatError extends DomainError {}
