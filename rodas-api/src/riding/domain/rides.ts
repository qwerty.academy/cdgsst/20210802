import CarType from "./car_type.ts";
import Coordinates from "./coordinates.ts";
import DistanceCalculator from "./distance_calculator.ts";

export default class Rides {
	static getOptions(
		cars: CarType[],
		pickup: Coordinates,
		dropoff: Coordinates,
		calculator: DistanceCalculator
	): RideOption[] {
		const distanceInKm = calculator.calculate(pickup, dropoff);
		return cars.map((c) => carToRideOption(c, distanceInKm));
	}
}

function carToRideOption(car: CarType, distanceInKm: number): RideOption {
	return {
		distanceInKm,
		rideType: car.name,
		estimatedCost: distanceInKm * car.costPerKm,
	};
}

export class RideOption {
	constructor(
		readonly estimatedCost: number,
		readonly distanceInKm: number,
		readonly rideType: string
	) {}
}
