import DomainError from "./domain-error.ts";
import { Either, success } from "../../shared/either.ts";

export default class Coordinates {
	readonly lat: number;
	readonly long: number;

	private constructor(value: string) {
		[this.lat, this.long] = value.split(",").map((v) => parseFloat(v));
	}

	static from(value: string): Either<Coordinates, InvalidcoordinatesFormatError> {
		return success(new Coordinates(value));
	}
}

class InvalidcoordinatesFormatError implements DomainError {
	error(): string {
		return "@InvalidCoordinatesFormat";
	}
}
