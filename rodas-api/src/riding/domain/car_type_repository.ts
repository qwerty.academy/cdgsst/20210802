import CarType from "./car_type.ts";

export default interface CarTypeRepository {
	getAll(): Promise<CarType[]>;
}
