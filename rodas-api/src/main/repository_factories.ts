import CarTypeRepository from "../riding/domain/car_type_repository.ts";
import InMemoryCarTypeRepository from "../infra/in_memory_repository/in_memory_car_type_repository.ts";

export function buildCarTypeRepository(): CarTypeRepository {
	return InMemoryCarTypeRepository.instance();
}
