import DistanceCalculator from "../riding/domain/distance_calculator.ts";
import LinearDistanceCalculator from "../adapters/linear_distance_calculator.ts";

export function buildDistanceCalculator(): DistanceCalculator {
	return new LinearDistanceCalculator();
}
