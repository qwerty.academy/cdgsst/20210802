import CarType from "../../riding/domain/car_type.ts";
import CarTypeRepository from "../../riding/domain/car_type_repository.ts";

export default class InMemoryCarTypeRepository implements CarTypeRepository {
	private static _instance: InMemoryCarTypeRepository = new InMemoryCarTypeRepository();

	static instance(): InMemoryCarTypeRepository {
		return this._instance;
	}

	private db: CarType[];

	private constructor() {
		this.db = [new CarType("SUV", 1500), new CarType("ECO", 1250)];
	}

	getAll(): Promise<CarType[]> {
		return Promise.resolve(this.db);
	}
}
