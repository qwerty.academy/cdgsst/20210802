import { Either, success, error } from "./either.ts";

export default interface UserPrincipal {
	getAccountId(): AccountId;
}

export class AccountId {
	static build(value: string): Either<AccountId, InvalidAccountIdError> {
		if (!value) return error(new InvalidAccountIdError());

		return success(new AccountId(value));
	}

	private constructor(readonly value: string) {}

	get(): string {
		return this.value;
	}
}

class InvalidAccountIdError {
	error() {
		return "@InvalidAccountId";
	}
}
