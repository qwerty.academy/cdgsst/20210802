export type Either<L, A> = Success<L, A> | Error<L, A>;

class Success<L, A> {
	constructor(readonly value: L) {}

	isSuccess(): this is Success<L, A> {
		return true;
	}

	isError(): this is Error<L, A> {
		return false;
	}
}

class Error<L, A> {
	constructor(readonly value: A) {}

	isSuccess(): this is Success<L, A> {
		return false;
	}

	isError(): this is Error<L, A> {
		return true;
	}
}

export const success = <L, A>(l: L): Either<L, A> => {
	return new Success<L, A>(l);
};

export const error = <L, A>(a: A): Either<L, A> => {
	return new Error<L, A>(a);
};
