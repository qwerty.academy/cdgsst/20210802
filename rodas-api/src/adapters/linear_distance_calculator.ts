import Coord from "../riding/domain/coord.ts";
import DistanceCalculator from "../riding/domain/distance_calculator.ts";

export default class LinearDistanceCalculator implements DistanceCalculator {
	calculate(pickup: Coord, dropoff: Coord): number {
		return this.getDistanceFromLatLonInKm(pickup.lat, pickup.long, dropoff.lat, dropoff.long);
	}

	private getDistanceFromLatLonInKm(lat1: number, lon1: number, lat2: number, lon2: number) {
		var R = 6371; // Radius of the earth in km
		var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
		var dLon = this.deg2rad(lon2 - lon1);
		var a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(this.deg2rad(lat1)) *
				Math.cos(this.deg2rad(lat2)) *
				Math.sin(dLon / 2) *
				Math.sin(dLon / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c; // Distance in km
		return d;
	}

	private deg2rad(degrees: number) {
		return degrees * (Math.PI / 180);
	}
}
