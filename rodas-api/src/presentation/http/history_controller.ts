import { OpineRouter, OpineRequest, OpineResponse } from "../../deps.ts";

const historyController = OpineRouter();

historyController.get("/", getHistory);
historyController.get("/-/count-by-year", countByYear);

function getHistory(_: OpineRequest, res: OpineResponse) {
	res.json([
		{
			rideId: "xxxds2",
			date: "2021-07-31T22:30",
			pickup: "-8.817956,13.232034",
			pickupAddress: "Largo do Ambiente S/N",
			pickupTime: "2021-07-31T22:23",
			dropoff: "-8.817976,13.232033",
			dropoffAddress: "R. dos Enganos 1",
			paymentMode: "CASH",
			rideType: "SUV",
			scheduledTime: "2021-07-31T22:30",
			driverId: "1fwq5t12",
			driverName: "Nelson Pio",
			driverRating: 4.5,
			status: "Finished",
			rideCost: 230.45,
			rideRate: 4.5,
			carModel: "Land Rover Defender",
			carLicensePlace: "LD-59-98-EA",
		},
	]);
}

function countByYear(_: OpineRequest, res: OpineResponse) {
	res.json([
		{
			year: 2021,
			count: 7,
		},
	]);
}

export default historyController;
