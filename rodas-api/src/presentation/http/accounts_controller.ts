import { OpineRouter, OpineRequest, OpineResponse, Status } from "../../deps.ts";

const accountsController = OpineRouter();

accountsController.delete("/:mobileNumber/places/:id", deletePlace);
accountsController.delete("/:mobileNumber/cards/:id", deleteCard);

accountsController.get("/:mobileNumber/-/otp", getOTP);
accountsController.get("/:mobileNumber/places", listPlaces);
accountsController.get("/:mobileNumber/cards", listCards);

accountsController.patch("/:mobileNumber/", editProfile);

accountsController.post("/", createAccount);
accountsController.post("/:mobileNumber/places", addPlace);
accountsController.post("/:mobileNumber/-/activate", activateAccount);
accountsController.post("/:mobileNumber/-/login", login);
accountsController.post(":mobileNumber/cards", addCreditCard);

function activateAccount(_: OpineRequest, res: OpineResponse) {
	res.headers?.append(
		"x-access-token",
		"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50IjoiMjQ0OTIyOTIzOTI0In0.QCrCsC2MXEdyz28iWzRkNwyq7vtiGoPpDGAVo2V534g"
	);

	res.sendStatus(Status.OK);
}

function addCreditCard(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function addPlace(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function createAccount(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function deleteCard(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function deletePlace(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function editProfile(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function getOTP(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function listPlaces(_: OpineRequest, res: OpineResponse) {
	res.json([
		{
			id: "sdndWd",
			label: "Home",
			address: "R. Mal Bros Tito, 85, 5F",
			coord: "-8.816956,13.232134",
		},
		{
			id: "sdndW5",
			label: "Home",
			address: "R. Missao 444, 4A",
			coord: "-8.817356,13.232134",
		},
	]);
}

function listCards(_: OpineRequest, res: OpineResponse) {
	res.json([
		{
			id: "ds2ms2",
			type: "visa",
			lastFourDigits: "3324",
			nameOnCard: "Ze de Povo",
			expirationDate: "12/23",
		},
	]);
}

function login(_: OpineRequest, res: OpineResponse) {
	activateAccount(_, res);
}

export default accountsController;
