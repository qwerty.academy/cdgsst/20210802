import { opine, Opine, opineJson } from "../../deps.ts";
import accountsController from "./accounts_controller.ts";
import historyController from "./history_controller.ts";
import ridesController from "./rides_controller.ts";

export default function startHttpServer(): Opine {
	const app = opine();

	app.use(opineJson());

	app.use("/v1/rides", ridesController);
	app.use("/v1/history", historyController);
	app.use("/v1/accounts", accountsController);

	// You can call listen the same as Express with just
	// a port: `app.listen(3000)`, or with any arguments
	// that the Deno `http.serve` methods accept. Namely
	// an address string, HttpOptions or HttpsOptions
	// objects.
	app.listen({ port: 3000 });
	console.log("Rodas API started on port 3000");

	return app;
}
