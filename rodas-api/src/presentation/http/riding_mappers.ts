import { RideOption } from "../../riding/domain/rides.ts";
import { Either, error, success } from "../../shared/either.ts";

export function toRideOptionsDto(option: RideOption): Record<number, unknown> {
	return { ...option };
}

type GetRideOptionsRequest = {
	pickup: string;
	dropoff: string;
};

export function toGetRideOptionsRequest({
	pickup,
	dropoff,
}: GetRideOptionsRequest): Either<{ pickup: string; dropoff: string }, false> {
	if (pickup && dropoff) return success({ pickup, dropoff });

	return error(false);
}
