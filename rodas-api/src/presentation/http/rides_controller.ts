import { OpineRouter, OpineRequest, OpineResponse, Status } from "../../deps.ts";
import RidingService from "../../riding/application/riding_service.ts";

import * as strategyFactories from "../../main/strategy_factories.ts";
import * as repositoryFactories from "../../main/repository_factories.ts";

import { toGetRideOptionsRequest, toRideOptionsDto } from "./riding_mappers.ts";

const ridesController = OpineRouter();

ridesController.delete("/:rideId", cancelRide);

ridesController.get("/-/current", currentRide);
ridesController.get("/:ridesId", getRide);
ridesController.get("/:rideId/status", rideStatus);

ridesController.post("/", bookRide);
ridesController.post("/-/options", listRidesOptions);

ridesController.put("/:rideId/ride-rate", rateRide);

function bookRide(_: OpineRequest, res: OpineResponse) {
	res.json({
		rideId: "xxw38x",
	});
}

function cancelRide(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function currentRide(_: OpineRequest, res: OpineResponse) {
	res.json({
		rideId: "31xxds8x",
	});
}

function getRide(_: OpineRequest, res: OpineResponse) {
	res.json({
		rideId: "xxxds2",
		date: "2021-07-31T22:30",
		pickup: "-8.817956,13.232034",
		pickupAddress: "Largo do Ambiente S/N",
		pickupTime: "2021-07-31T22:23",
		dropoff: "-8.817976,13.232033",
		dropoffAddress: "R. dos Enganos 1",
		paymentMode: "CASH",
		rideType: "SUV",
		scheduledTime: "2021-07-31T22:30",
		driverId: "1fwq5t12",
		driverName: "Nelson Pio",
		driverRating: 4.5,
		status: "Ongoing",
		rideCost: 230.45,
		rideRate: 4.5,
		carModel: "Land Rover Defender",
		carLicensePlace: "LD-59-98-EA",
	});
}

async function listRidesOptions(req: OpineRequest, res: OpineResponse) {
	const getRideOptionsRequestOrFalse = toGetRideOptionsRequest(req.body);

	if (getRideOptionsRequestOrFalse.isError()) return res.sendStatus(Status.BadRequest);

	const { pickup, dropoff } = getRideOptionsRequestOrFalse.value;

	const result = await buildRidingService().getRideOptions(pickup, dropoff);

	res.json(result.map(toRideOptionsDto));
}

function rateRide(_: OpineRequest, res: OpineResponse) {
	res.sendStatus(Status.OK);
}

function rideStatus(_: OpineRequest, res: OpineResponse) {
	res.json({ status: "DriverOnTheWay", driverPosition: "-8.817976,13.232033" });
}

function buildRidingService() {
	return new RidingService(
		strategyFactories.buildDistanceCalculator(),
		repositoryFactories.buildCarTypeRepository()
	);
}

export default ridesController;
