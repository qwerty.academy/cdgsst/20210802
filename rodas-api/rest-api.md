# Rodas REST API

API for rodas backend access from official specs provided by ZAFIR,
related documents and test files.
Emphasis on parsing and writing robustness, cross-format feature compatibility
with a unified resource representation.

## Method Invocation

Aside from signup and login methods all requests must have the "x-access-token"
parameter name either in the query or in the request header:

e.g: /v1/accounts/me?x-access-token=some_acess_token

Request from third party integration API should user the "x-api-token" parameter
to run in a more restricted sandbox

## Accounts API

API to use with customer apps.

### Signup Process

-   Send account information
-   Retrieve confirmation code sent by sms
-   Send the confirmation code to finish the process

### Login Process

-   Send phone number
-   Retrieve confirmation code sent by sms
-   Request access token with the confirmation code

### Create Account / Signup

**Request**

    POST /v1/accounts

```json
/* Request Payload */
{
	"mobileNumber": "string",
	"firstName": "string",
	"lastName": "string",
	"email": "string"
}
```

**Response**

If everything is OK HTTP 200 Code is sent and confirmation code sent to client mobile

| Code | Description               |
| ---- | ------------------------- |
| 200  | Everything is ok          |
| 400  | Request payload malformed |

In case of bad request, response payload should be, for example:

```json
{
	"firstName": "@NotNull"
}
```

Bad request error codes:

-   @NotNull
-   @Email
-   @Pattern
-   etc.

### Activate Account

This method activate a user account and retrive the JWT token

**Request**

    POST /v1/accounts/[mobileNumber]/-/activate

```json
/* Request payload */
{
	"otp": "xxxx"
}
```

**Response**

If everything is OK HTTP 200 Code is sent and confirmation code sent to with a JWT Token in the response x-access-token header parameter to be used in future requests in x-access-token request header

| Code | Description                    |
| :--: | ------------------------------ |
| 200  | Everything is ok               |
| 401  | invalid activation code        |
| 404  | mobileNumber is not registered |

### Get OTP

**Request**

    GET /v1/accounts/[mobileNumber]/-/otp

**Response**

If everything is OK HTTP 200 Code is sent and confirmation code sent to client mobile

| Code | Description      |
| :--: | ---------------- |
| 200  | Everything is ok |

### Login

Use the otp sent by the previous method to authenticate the client.

**Request**

    POST /v1/accounts/[mobileNumber]/-/login

```json
/* Request payload */
{
	"otp": "xxxx"
}
```

**Response**

If everything is OK HTTP 200 Code is sent and confirmation code sent to with a JWT Token in the response x-access-token header parameter to be used in future requests in x-access-token request header

### Edit Profile

**Request**

    PATCH /v1/accounts/[mobileNumber]

```json
/* Request payload */
{
	"firstName": "xxxx", // string, optional
	"lastName": "xxxx", // string, optional
	"dateOfBirth": "xxxx", // string [yyy-MM-dd], optional
	"defaultPaymentMode": "xxxx" // string, optional
}
```

### Add Place

**Request**

    POST /v1/accounts/[mobileNumber]/places

```json
/* Request payload */
{
	"label": "xxxx", // string
	"address": "xxx", // string
	"coord": "xxxx" // string e.g: -8.817956,13.232034
}
```

### List Places

**Request**

    GET /v1/accounts/[mobileNumber]/places

**Response**

```json
[
	{
		"id": "xxxx", // string
		"label": "xxxx", // string
		"address": "xxxx", // string
		"coord": "xxxx" // string e.g: -8.817956,13.232034
	}
]
```

### Delete Place

**Request**

    DELETE /v1/accounts/[mobileNumber]/places/[id]

**Response**

| Code | Description      |
| :--: | ---------------- |
| 200  | Everything is ok |
| 401  | invalid place    |

### Add Credit Card

**Request**

    POST /v1/accounts/[mobileNumber]/cards

```json
/* Request payload */
{
	"type": "visa|mastercard", // string
	"number": "xxxxx", // string
	"nameOnCard": "xxxx", //string
	"expirationDate": "xx/xx", // string [MM/yy]
	"cvv": "xxx" // string
}
```

### List Cards

**Request**

    GET /v1/accounts/[mobileNumber]/cards

**Response**

```json
[
	{
		"id": "xxx", // string
		"type": "visa|mastercard", // string
		"lastFourDigits": "xxxx", // string
		"nameOnCard": "xxxx", //string
		"expirationDate": "xx/xx" // string [MM/yy]
	}
]
```

### Delete Card

**Request**

    DELETE /v1/accounts/[mobileNumber]/cards/[id]

**Response**

| Code | Description      |
| :--: | ---------------- |
| 200  | Everything is ok |
| 401  | invalid card     |

## Rides API

### List Ride Options

**Request**

    POST /v1/rides/-/options

```json
/* Request payload */
[
	{
		"pickup": "xxx", // string [coordinates]
		"dropoff": "xxx", // string [coordinates]
		"paymentMode": "string"
	}
]
```

**Response**

```json
{
	"rideType": "xxx", // string
	"distanceInKm": 2.3, // number
	"estimatedCost": 4.5 // number
}
```

### Book Ride

**Request**

    POST /v1/rides

```json
/* Request payload */
{
	"pickup": "xxx", // string [coordinates]
	"pickupAddress": "xxx", // string
	"dropoff": "xxx", // string [coordinates]
	"dropoffAddress": "xxx", // string
	"paymentMode": "xxx", // string
	"rideType": "xxx", // string
	"scheduledTime": "xxx" // string [now|yyyy-MM-ddTHH:mm]
}
```

**Response**

| Code | Description          |
| :--: | -------------------- |
| 200  | Ok                   |
| 400  | Invalid request data |

```json
/* Ok Payload */
{
	"rideId": "xxx" // string
}
```

### Cancel Ride

**Request**

    DELETE /v1/rides/[rideId]

**Response**

| Code | Description     |
| :--: | --------------- |
| 200  | Ok              |
| 404  | Invalid ride id |

### Current Ride

Return the id of an ongoing ride

**Request**

    GET /v1/rides/-/current

**Response**

| Code | Description     |
| :--: | --------------- |
| 200  | Ok              |
| 404  | No ride ongoing |

```json
/* Ok Payload */
{
	"rideId": "xxx" // string
}
```

### Get Ride

**Request**

```
GET /v1/rides/[ridesId]
```

**Response**

```json
/* Ok payload */
{
	"rideId": "xxx", // string
	"date": "xxx", // string [yyyy-MM-ddTHH:mm]
	"pickup": "xxx", // string [coordinates]
	"pickupAddress": "xxx", // string
	"pickupTime": "xxx", // string [now|yyyy-MM-ddTHH:mm], opcional
	"dropoff": "xxx", // string [coordinates]
	"dropoffAddress": "xxx", // string
	"dropoffTime": "xxx", // string [now|yyyy-MM-ddTHH:mm], opcional
	"paymentMode": "xxx", // string
	"rideType": "xxx", // string
	"scheduledTime": "xxx", // string [now|yyyy-MM-ddTHH:mm]
	"driverId": "xxx", // string, opcional
	"driverName": "xxx", // string, opcional
	"driverRating": 4.5, // number, optional
	"status": {}, // object, see "Ride Status"
	"rideCost": 230.45, // number, opcional
	"rideRate": 4.5, // number, optional
	"carModel": "xxx", // string, opcional
	"carLicensePlace": "xxx" // string, opcional
}
```

### Ride Status

**Request**

    GET /v1/rides/[rideId]/status

**Response**

```json
/* Ok payload */
{
    "status": "Scheduled" | "FindingDrivers" | "DriverOnTheWay" | "Ongoing" | "Canceled" | "Finished" | "AwaintingRate",
    "driverPosition": "xxx", // string [coordinates]
}
```

### Rate Ride

**Request**

```
PUT /v1/rides/[rideId]/ride-rate
```

```json
/* Request payload */
{
	"rideRate": 4.5 // number
}
```

## History API

### Get History

Return past rides for a period ordered by date (descending order)

```
GET /v1/history?filter=[]
```

**Filter Parameter Values**

| Values         | Description                  |
| -------------- | ---------------------------- |
| this-month     | Return this month's rides    |
| this-trimester | Return this month's rides    |
| this-year      | Return this year's rides     |
| [yyyy]         | Return rides for [yyyy] year |

**Response**

```json
/* Ok payload */
[
	{
		"rideId": "xxx", // string
		"date": "xxx", // string [yyyy-MM-ddTHH:mm]
		"pickup": "xxx", // string [coordinates]
		"pickupAddress": "xxx", // string
		"pickupTime": "xxx", // string [now|yyyy-MM-ddTHH:mm]
		"dropoff": "xxx", // string [coordinates]
		"dropoffAddress": "xxx", // string
		"dropoffTime": "xxx", // string [now|yyyy-MM-ddTHH:mm]
		"paymentMode": "xxx", // string
		"rideType": "xxx", // string
		"scheduledTime": "xxx", // string [now|yyyy-MM-ddTHH:mm]
		"driverId": "xxx", // string
		"driverName": "xxx", // string
		"driverRating": 4.5, // number
		"rideCost": 230.45, // number
		"rideRate": 4.5, // number
		"carModel": "xxx", // string
		"carLicensePlace": "xxx" // string
	}
	//...
]
```

### Count by Year

**Request**

```
GET /v1/history/-/count-by-year
```

**Response**

```json
[
    {
        "year": number,
        "count": number,
    }
]
```
