import StringCalculator from "./StringCalculator"

describe("Implementacao do string calculator", () => {
    const calculator = new StringCalculator()

    test("for an empty string it will return 0", () => {

        const result = calculator.Add("");

        expect(result).toStrictEqual(0);
    })

    test("for '1' it will return 1", () => {

        const result = calculator.Add("1");

        expect(result).toStrictEqual(1);
    })

    test("for '1,2' it will return 3", () => {

        const result = calculator.Add("1,2");

        expect(result).toStrictEqual(3);
    })

    test("for '4,9' it will return 13", () => {

        const result = calculator.Add("4,9");

        expect(result).toStrictEqual(13);
    })

    test("for '3,7' it will return 10", () => {

        const result = calculator.Add("3,7");

        expect(result).toStrictEqual(10);
    })

    test("for '7' it will return 10", () => {

        const result = calculator.Add("7");

        expect(result).toStrictEqual(7);
    })

    test("for '71,57' it will return 128", () => {

        const result = calculator.Add("71,57");

        expect(result).toStrictEqual(128);
    })

    test("for '57' it will return 57", () => {

        const result = calculator.Add("57");

        expect(result).toStrictEqual(57);
    })

    test("for '5,7,9' it will return 21", () => {

        const result = calculator.Add("5,7,9");

        expect(result).toStrictEqual(21);
    })

    test("for '5,7,9,100' it will return 121", () => {

        const result = calculator.Add("5,7,9,100");

        expect(result).toStrictEqual(121);
    })

    test("for '5,7,3\n100' it will return 115", () => {

        const result = calculator.Add("5,7,3\n100");

        expect(result).toStrictEqual(115);
    })
});