export default class StringCalculator {
    Add(numbers: string): number {
        if(this.emptyString(numbers))
            return 0;
        
        const splited = this.splitTheString(numbers);

        return sumTheParts(splited);        
    }

    private splitTheString(numbers: string) {
        return numbers.split(/[,\n]/);
    }

    private emptyString(numbers: string) {
        return numbers === "";
    }
}

function sumTheParts(splited: string[]): number {
    const numbersMap = splited.map((v) => parseInt(v))
    return numbersMap
        .reduce((acc, cur) => acc+cur)

    
}
